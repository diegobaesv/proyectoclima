<?php
include('../resources/config.php');
$tipo = $_POST['sel_tipo'];
$area = $_POST['sel_area'];
$intervalo = $_POST['sel_intervalo'];
if(($tipo === "C01" || $tipo === "C13") && $area === "Peru"){
    $fecha = $_POST['sel_fecha'];    
}else{
    $fecha = date('Y-m-d');
    //$fecha = date('2023-12-18');
}
?>
<div id="slideshow" class="cycle-slideshow"                          
    data-cycle-log="false"
    data-cycle-timeout="1000"
    data-cycle-fx="none"
    data-cycle-pager="#nav"
    data-cycle-slides="> a">
    <?php
    $imagenes = _listImage($tipo,$area,$fecha,$intervalo);
    if(count($imagenes)>0){
        foreach ($imagenes as $key => $value) {
            $label = $value['label'];
            $files = str_replace("../","",$value['file']);
            echo 
            "<a href='".$files."' target='_blank' data-cycle-pager-template='<a href=#>$label</a>'>
                <img src='".$files."' title='$label' class='img-responsive first' width='100%' />
            </a>";	
        }
    }else{
        echo '<div class="text-center h3">Sin imágenes disponibles.</div>';
    }
    ?>
</div>
<div class="">
    <div id="nav" class="nav justify-content-center"> </div>
</div>