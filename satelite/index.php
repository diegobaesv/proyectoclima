        <?php
        include('resources/config.php');
        ?>
        <h1 class="h3 mb-4 text-gray-800">Imágenes de satélite</h1>
        <div>
            <div class="row">
                <div class="col-md-3">
                    <div class="mt-0">
                        <label class="m-0 font-weight-bold text-primary">Tipo</label>    
                        <select name="tipo" id="tipo" class="custom-select">
                        <?php
                        foreach(_typeImage() as $id => $imagen) {
                            echo "<option value=\"".$id."\"> ".$imagen['label']."</option>";
                        }
                        ?>
                        </select>
                    </div>
                    <div class="mt-3">
                        <label class="m-0 font-weight-bold text-primary">Área</label>     
                        <select name="area" id="area" class="custom-select">
                        <?php
                        foreach(_areaImage() as $id => $area) {
                            echo "<option value=\"".$id."\"> ".$area['label']."</option>";
                        }
                        ?>
                        </select>
                    </div>
                    <div class="mt-3" id="archivo">
                        <label class="m-0 font-weight-bold text-primary">Archivo</label>
                        <select name="fecha" id="fecha" class="custom-select">
                        </select>
                    </div>
                    <div class="mt-3 mb-4">
                        <label class="m-0 font-weight-bold text-primary">Intervalo</label>
                        <?php
                            foreach(_intervaloImage() as $id => $intervalo) {
                                echo 
                                "<div class=\"custom-control custom-radio\" id=\"u$id\">
                                    <input type=\"radio\" id=\"inter$id\" name=\"intervalo\" value=".$id." class=\"custom-control-input\">
                                    <label class=\"custom-control-label\" for=\"inter$id\">".$intervalo['label']."</label>
                                 </div>";                                
                            }
                        ?>
                    </div> 
                    <div class="mt-3 mb-4">
                        <label class="m-0 font-weight-bold text-primary">Animación </label><br>
                        <div class="btn-group" role="group">
                            <button data-cycle-cmd="prev" data-cycle-context="#slideshow" class="btn btn-primary media mr-1" title="<<">
                                <i class="fa fa-backward"></i>
                            </button>
                            <button data-cycle-cmd="pause" data-cycle-context="#slideshow" class="btn btn-primary media mr-1" title="Stop">
                                <i class="fa fa-pause"></i>
                            </button>
                            <button data-cycle-cmd="resume" data-cycle-context="#slideshow" class="btn btn-primary media mr-1" title="Play">
                                <i class="fa fa-play"></i>
                            </button>
                            <button data-cycle-cmd="next" data-cycle-context="#slideshow" class="btn btn-primary media mr-1" title=">>">
                                <i class="fa fa-forward"></i>
                            </button>
                        </div>
                    </div>                    
                </div>

                <div class="col-md-9 mb-5">
                    <div id="slider" class="text-center">

                    </div> 
                </div>
            </div>
        </div>