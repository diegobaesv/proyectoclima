<?php
const PATH = "../../imagensat/";

function _typeImage($type=array()){
    $image['C09'] 	 = array('label'=>'Vapor de agua (nivel medio)');
    $image['C01'] 	 = array('label'=>'Visible');
    $image['C13'] 	 = array('label'=>'Infrarrojo');
	$image['InPrec'] = array('label'=>'Intensidad de precipitación');
	$image['Rayos']  = array('label'=>'Rayos');	
	return $image;
}

function _areaImage($area=array()){
    $area['Peru'] = array('label'=>'Perú');
    $area['Lima'] = array('label'=>'Lima');
    $area['PeruNorte']  = array('label'=>'Norte');
	$area['PeruCentro'] = array('label'=>'Centro');
	$area['PeruSur'] 	= array('label'=>'Sur');	
	return $area;
}

function _fileImage($tipo,$area,$date=''){
//  imagensat\C09\Lima_C09_202312181930.gif
	$file = array();
    $date = ($date!='')? date($date): date('Y-m-d');
	$i = 0;
	$n = 0;
	while ($i <= 10){
		$Ymd = fwSuma_fechas($date,-$i);
		$day = substr($Ymd,-2);
		$month = substr($Ymd,5,2);
		$year  = substr($Ymd,0,4);

		$glob = glob(PATH.$tipo."/".$area."_".$tipo."_".$year.$month.$day."*");
		if(count($glob)>0){
			$lblYmd = $day." ".substr(fwMes($month),0,3).". ".$year;
			$file[$n++]=array('Ymd'=>$Ymd,'label'=>$lblYmd . ($Ymd == date('Y-m-d')? ' (Hoy día)': ''));
		}

		if ($n==3){break;}
        $i++;
    }
    return $file;
}

function _intervaloImage(){
    $inervalo[2]  = array('label'=>'Últimas 2 horas');
    $inervalo[6]  = array('label'=>'Últimas 6 horas');
    $inervalo[12] = array('label'=>'Últimas 12 horas');
	$inervalo[24] = array('label'=>'Todas');
	return $inervalo;
}

function _listImage($tipo,$area,$date,$intervalo){
//	Peru_C09_2023 12 18 1930.gif
	$data = array();
	$date = date($date);
	$i = 0;
	$n = 0;
	$f = 0;
	$status = true;
	while ($status == true){
		$fecha = fwSuma_fechas($date,-$i);
		$day   = substr($fecha,-2);
		$month = substr($fecha,5,2);
		$year  = substr($fecha,0,4);		
		for($h=23; $h>=0; $h--){
			$hh = str_pad($h,2,'0',STR_PAD_LEFT);
			$m = 50;
			while ($m>=0){
				$mi = str_pad($m,2,'0',STR_PAD_LEFT);
				$label = fwFecha_utclocal($year,$month,$day,$hh).":".$mi;
				$file  = PATH.$tipo."/".$area."_".$tipo."_".$year.$month.$day.$hh.$mi.".gif";
				if (file_exists($file)){
					$data[$f++]=array('file'=>$file,'label'=>$label);					
				}
				$m-=10;
				if ($n==$intervalo){$status = false; break;}
			}
			if ($n==$intervalo){$status = false; break;}
			
			$n++;
		}
		if ($n==$intervalo){$status = false; break;}
		$i++;
	}
	return $data;
}

function fwMes($mes){
	switch ($mes){
		case '01':	$name_mes="Enero";		break;
		case '02':	$name_mes="Febrero";	break;
		case '03':	$name_mes="Marzo";		break;
		case '04':  $name_mes="Abril";		break;
		case '05':	$name_mes="Mayo";		break;
		case '06':	$name_mes="Junio";		break;
		case '07':	$name_mes="Julio";		break;
		case '08':	$name_mes="Agosto";		break;
		case '09':	$name_mes="Septiembre";	break;
		case '10':  $name_mes="Octubre";	break;
		case '11':  $name_mes="Noviembre";	break;
		case '12':  $name_mes="Diciembre";	break;
   }
   return $name_mes;
}

function fwSuma_fechas($fecha,$ndias){
	$nuevafecha = strtotime ( $ndias.' day' , strtotime ( $fecha ) ) ;
	$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
	return $nuevafecha;
}

function fwFecha_utclocal($year,$month,$day,$hh){
	$date = fwSuma_fechas($year."-".$month."-".$day,-1);
	if($hh==0){$hora = 19;
	}elseif($hh==1){$hora = 20;
	}elseif($hh==2){$hora = 21;
	}elseif($hh==3){$hora = 22;
	}elseif($hh==4){$hora = 23;
	}else{
		$hora = $hh - 5;
		$date = $year."-".$month."-".$day;
	}
	$year = substr($date,0,4);
	$month = substr($date,5,2);
	$day = substr($date,-2);	
	$hora = substr("00$hora",-2);
	$label = $day." ".substr(fwMes($month),0,3).". ".$year. " ".$hora;
	return $label;
}
?>