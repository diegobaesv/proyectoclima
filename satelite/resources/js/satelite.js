$(document).ready(function () {
    var g_tipo = "C09";
    var g_area = "Peru";
    var g_intervalo = "2";
    var g_index = "";

    getDefault();

    var myInterval = setInterval(refrescar, 900000);
	
    $("#area, #tipo").change(function () {
        getArchivo(g_index);
    });

    $("#fecha").change(function () {
        refrescar();
    });

    $("input:radio[name='intervalo']").change(function () {
        refrescar();
    });

    function refrescar() {
        getImagen($("#tipo").val(),$("#area").val());	
	}

    function getImagen(tipo,area){
        let fecha = $("#fecha").val();
        let intervalo = $("input:radio[name='intervalo']:checked").val();

        $("#slider").load('satelite/include/ajx_imagen.php', {sel_tipo: tipo, sel_area: area, sel_fecha: fecha, sel_intervalo: intervalo}, function () {
            $('.cycle-slideshow').cycle();
        });

        clearInterval(myInterval);
		myInterval = setInterval(refrescar, 900000);
    }

    function getArchivo(index){
        let tipo = $("#tipo").val();
        let area = $("#area").val();
        $.ajax({
            url: 'satelite/include/ajx_archivo.php',
            data: {sel_tipo: tipo, sel_area: area, fecha: index},
            type: 'POST',
            dataType: 'html',
            success: function (response) {
                if(response === '-555'){
                    $("#fecha").html('');
                    $("#archivo").addClass("d-none");
                    $("#u24").addClass("d-none");
                    if($("input:radio[name='intervalo']:checked").val() === '24'){
                        $("input:radio[name='intervalo'][value='"+ g_intervalo +"']").prop('checked',true);
                    }
                }else{
                    $("#archivo").removeClass("d-none");
                    $("#fecha").html(response).show();
                    $("#fecha")[0].selectedIndex = 0;
                    $("#u24").removeClass("d-none");
                }
                getImagen(tipo,area);
            }
        });
    }

    function getDefault() {
        $("#tipo option[value="+ g_tipo +"]").attr("selected",true);
        $("#area option[value="+ g_area +"]").attr("selected",true);
        $("input:radio[name='intervalo'][value='"+ g_intervalo +"']").prop('checked',true);        
        getArchivo(g_index);
    }    
});